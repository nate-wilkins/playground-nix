# Playground Nix

## Development

1. Clone.

### Nix

1. Get `nix`.
2. `nix develop --extra-experimental-features 'flakes nix-command' .`
    **NOTE:** This will work when you have docker installed. You should look into `--ignore-environment` for `nix develop`.
    1. `docker load < result`
    2. `docker run --rm playground-nix:latest`
    3. `gunzip --stdout result > result.tar`
    4. `dive docker-archive://result.tar`

## Docker

1. Get `docker`.
2. `docker build -t playground-nix-docker:latest .`
3. `docker run --rm playground-nix-docker:latest`
4. `dive playground-nix-docker:latest`
