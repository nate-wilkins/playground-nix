{
  pkgs,
  manifest,
  ...
}: (
  let
    name                                           = manifest.name;
    tag                                            = "latest";
  in (
    pkgs.dockerTools.buildLayeredImage {
      inherit name tag;

      contents = [
        pkgs.bash
      ];

      config = {
        Entrypoint = [ "${pkgs.bash}/bin/bash" ];
      };
    }
  )
)
